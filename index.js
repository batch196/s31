/*
What is a client?

	A client is an application which creates requests for resources from
	a server. A client will trigger an action in the web development context, through a 
	URL and wait for the response of the server.

What is server?

	A server is able to host and deliver resources by a client. In fact,
	a single server can handle multiple clients.

Wha is Node.js?

	Node.js is a runtime environment which allows us to create/develop backend/
	server-side applications with javascript.

npm - Node Package Manager

*/

// console.log("hello world");

let http = require("http");
// console.log(http);
//require is a built in js method w/c alloes us to import packages.
//http - is a default package from nodejs
//http is a module. modules are packages that we imported
//the http module let us create a server which is able to communicate with a client through the use of hyper text transfer 
//protocol
//protocol to client-server communication - http://localhost:4000 - server/application
http.createServer(function(request,response){
// console.log(request.url);//contains endpoint

	if(request.url ==="/"){
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end("Hello from our first server! This is from / endpoint");
} else if(request.url === "/profile"){
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end("Hello im Zandro");
}



}).listen(4000);

console.log("Server is running on localHost:4000!");












